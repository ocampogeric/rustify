use termion::clear;
use termion::color;
use rspotify::spotify::client::Spotify;
use rspotify::spotify::util::get_token;
use rspotify::spotify::oauth2::{SpotifyClientCredentials,SpotifyOAuth};

use app::utils;

#[allow(dead_code)] 
pub fn setup() {
    println!("{}{}Welcome to Rustify :){}",
        color::Bg(color::Green),
        color::Fg(color::White),
        color::Bg(color::Reset));
        utils::create_menu(utils::Menu::Principal);
    // println!("{}", clear::All);
    /*match check_auth() {
        Some(spotify) => {
            utils::create_menu(utils::Menu::Principal);
            let tracks = spotify.current_user_saved_tracks(10, 0);
            match tracks {
                Ok(v) => {
                    // println!("{:?}", v.items);
                    for item in &v.items {
                        println!("===> {:?}", &item.track.name);
                    }
                },
                Err(e) => println!("error parsing header: {:?}", e),
            }
        },
        None => {
            println!("An error occurs on get token!");
        }
    };*/
}
#[allow(dead_code)]
pub fn check_auth() -> Option<Spotify> {
    let mut oauth = SpotifyOAuth::default()
    .scope("user-library-read")
    .build();
    match get_token(&mut oauth) {
        Some(token_info) => { 
            let client_credential = SpotifyClientCredentials::default()
            .token_info(token_info)
            .build();
            // Or set client_id and client_secret explictly
            // let client_credential = SpotifyClientCredentials::default()
            //     .client_id("this-is-my-client-id")
            //     .client_secret("this-is-my-client-secret")
            //     .build();
            let spotify = Spotify::default()
            .client_credentials_manager(client_credential)
            .build();


            return Some(spotify);
            /*let tracks = spotify.current_user_saved_tracks(10, 0);
            match tracks {
                Ok(v) => {
                    // println!("{:?}", v.items);
                    for item in &v.items {
                        println!("===> {:?}", &item.track.name);
                        // println!(item.tracks)
                    }
                },
                Err(e) => println!("error parsing header: {:?}", e),
            }*/
        },
        None => println!("Fail")
    };
    return None;
    
}
