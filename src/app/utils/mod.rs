use termion::clear;
use termion::raw::IntoRawMode;
use termion::color;
use std::io::{Write, stdout, stdin};
pub enum Menu {
    Library,
    Playlist,
    Principal
}

pub fn create_menu(menu: Menu) {
    match menu {
        Menu::Principal => {
            let stdin = stdin();
            let mut stdin = stdout().into_raw_mode().unwrap();
            
            write!(stdout(), "{}{}{}",
                // clear the screen
                termion::clear::All,
                // goto (1,1)
                termion::cursor::Goto(3,1),
                // hide Cursor
                termion::cursor::Hide
                ).unwrap();
            let termsize = termion::terminal_size().ok();
            let termwidth = termsize.map(|(w,_)| w-2).unwrap();
	        let termheight = termsize.map(|(_,h)| h-1).unwrap();

            for h in 1..termheight+1 {
                print!("_")
            }
            println!("{}", clear::AfterCursor);
            stdout().flush().unwrap();

        },
        Menu::Library => {
            println!("Create my saved songs menu...");
        },
        Menu::Playlist=> {
            println!("Create playlist menu...");
        }
    }
}


